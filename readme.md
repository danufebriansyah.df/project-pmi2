# SIAM IT-PMI

A web-based asset management system program that helps to record office items, with a basic user interface and few fundamental adjustments; it is envisaged that the application may be used by a variety of organizations.

## Prerequisites

-  PHP>=5.6.0
-  Codeigniter>=3.10.0
-  XAMPP>=5.6.0

## Installation Procedure

- Download or clone the repository and generate an extraction folder; 
- Place the extracted file in the htdocs folder inside the XAMPP folder; 
- Activate Apache and MySQL in the XAMPP Control Panel; 
- Import sql file from an extraction file using phpmyadmin database folder into 
  phpmyadmin; 
- Open phpmyadmin into http://localhost/phpmyadmin
- Create new database for this project
- Lastly, you may continue with this project by running localhost with the 
 same 
  folder name.

## Under Development 
This website is still in the process of being developed to offer better service and emphasize the website's functions for Biro Teknologi Informasi PMI Pusat.

## License

[MIT](https://choosealicense.com/licenses/mit/)